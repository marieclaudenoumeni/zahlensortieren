package com.example.p_zahlensortieren;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.example.p_zahlensortieren.Sortieren;

public class MainActivity extends AppCompatActivity {

    private int[] value;
    //final EditText editText = (EditText) findViewById(R.id.editText);


    // Instanziieren der anonymen Implementierung des OnClickListener-Interfaces
    View.OnClickListener onButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            // innerhalb dieser Methode kann auf den Klick reagiert werden
            Sortieren s = new Sortieren();
             String result;
             result = s.Sort();
            value = new int[20];
            int a = 0;
           //sortiereArray(value);

            Context context = getApplicationContext();
            CharSequence text = "Hello toast!";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, result, duration);
            toast.show();
            //Toast.makeText(this@MainActivity,value, Toast.LENGTH_SHORT).show()

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button = (Button) findViewById(R.id.button);
        button.setOnClickListener(onButtonClickListener);

        final TextView textView = (TextView) findViewById(R.id.textView);
        //textView.addTextChangedListener(new TextWatcher());



    }
}
