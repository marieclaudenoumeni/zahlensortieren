package com.example.p_zahlensortieren;

import java.util.Random;
import java.util.Arrays;


public class Sortieren {

    private int[] zahlen;

    public static void main(String[] args) {
        new Sortieren();
    }

    public String Sort()            {
        zahlen = new int[20];
        erzeugeZufallszahlen();
        sortiereArray(zahlen); //   sortiereArray(int[])
        return zahlen.toString();
    }

    public void erzeugeZufallszahlen(){
        Random zufallsZahl = new Random();
        for(int i=0;i<zahlen.length;i++){
            zahlen[i] = zufallsZahl.nextInt(98)+1;
        }
    }

    public void sortiereArray(int [] nr){
        int zahl = nr.length;
        for (int wieviel = 1; wieviel < zahl; wieviel++){
            for(int ubrigeZahlen = 0; ubrigeZahlen < zahl - wieviel; ubrigeZahlen++){
                if(nr[ubrigeZahlen] > nr [ubrigeZahlen+1]){
                    int temp = nr[ubrigeZahlen];
                    nr[ubrigeZahlen] = nr [ubrigeZahlen+1];
                    nr[ubrigeZahlen+1] = temp;
                }
            }
        }
        System.out.println(zahlen[zahl]);  //System.out.println(zahlen[i])
    }
}
